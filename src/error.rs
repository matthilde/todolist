/*
 * todolist by matthilde
 *
 * src/error.rs
 *
 * Error structure
 */
use std::{ io, fmt };

#[allow(dead_code)]
#[derive(Debug)]
pub enum TodoError {
    IO(io::Error),
    StackUnderflow,
    StackOutOfRange,
    NoTask,
    ParseError,
    ArgMissing,
    UnknownError,
}

pub type Result<T> = std::result::Result<T, TodoError>;

impl fmt::Display for TodoError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TodoError::IO(e) => e.fmt(f),
            TodoError::StackUnderflow  => write!(f, "stack underflow"),
            TodoError::StackOutOfRange => write!(f, "stack index out of range"),
            TodoError::NoTask          => write!(f, "task list is empty"),
            TodoError::ParseError      => write!(f, "could not parse value"),
            TodoError::ArgMissing      => write!(f, "missing argument"),
            TodoError::UnknownError    => write!(f, "unknown error"),
        }
    }
}

impl From<io::Error> for TodoError {
    fn from(err: io::Error) -> TodoError {
        TodoError::IO(err)
    }
}
