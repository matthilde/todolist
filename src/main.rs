/*
 * todolist by matthilde
 *
 * src/main.rs
 *
 * Main program
 */
use std::{ io, env, path };
use crossterm::{
    execute,
    style::{ Attribute, Color, Print, SetAttribute, SetForegroundColor }
};

use todolist::{ Result, TodoError };
use todolist::Todo;

fn display_reminder(todolist: &Todo) -> Result<()> {
    let task = match todolist.current_task() {
        Ok(s) => s,
        Err(TodoError::NoTask) => "No current task.".to_string(),
        Err(e) => return Err(e)
    };
    execute!(
        io::stdout(),
        SetAttribute(Attribute::Bold),
        SetForegroundColor(Color::Blue),
        Print("TODO: "),
        SetAttribute(Attribute::Reset),
        Print(task),
        Print("\n".to_string())).unwrap();
    Ok(())
}

fn display_whole_list(todolist: &Todo) -> Result<()> {
    display_reminder(&todolist)?;
    println!("");

    for (i, task) in todolist.stack.iter().rev().enumerate() {
        execute!(
            io::stdout(),
            SetAttribute(Attribute::Bold),
            SetForegroundColor(Color::Blue),
            Print(format!("[{}] ", i + 1)),
            SetAttribute(Attribute::Reset),
            Print(task),
            Print("\n")
        )?;
    }

    Ok(())
}

fn display_help() {
    println!("USAGE: todolist (help|add|addp|swap|done|reset) [arg]");
}

fn main() -> Result<()> {
    let argv: Vec<String> = env::args().collect();

    // let todolist_path = path::Path::new(".todolist");
    let mut path = path::PathBuf::new();
    match env::var("TODOFILE") {
        Ok(val) => { path.push(val); },
        Err(_) => {
            let home = env::var("HOME").or::<String>(Ok(".".to_string())).unwrap();
            path.push(home);
            path.push(".todolist");
        }
    }
    let mut todolist = Todo::new(&path)?;

    let args = &argv[1..];
    if args.is_empty() {
        display_reminder(&todolist)?;
        return Ok(());
    }

    let command = args[0].as_str();
    match command {
        "help" => { display_help(); },
        "add" | "addp" => { 
            if args.len() >= 2 {
                let element = args[1..].join(" ");
                todolist.add(element.trim(), command == "addp");
            } else {
                return Err(TodoError::ArgMissing);
            }
        },
        "swap" => { 
            if args.len() >= 2 {
                let iarg = args[1].parse().map_err(|_| TodoError::ParseError)?;
                todolist.swap(iarg)?;
            } else {
                todolist.swap(2)?;
            }
        },
        "done" => {
            if let Some(task) = todolist.done() {
                println!("Task '{}' done.", task);
            } else {
                println!("There are no tasks to be done.");
            }
        },
        "all"  => { display_whole_list(&todolist)?; },
        "reset" => { todolist.reset(); },
        _      => { display_help(); }
    };

    todolist.save()?;

    Ok(())
}
