/*
 * todolist by matthilde
 *
 * src/lib.rs
 *
 * Core functionality of the program
 */
use std::{
    io::{ self, BufReader, BufRead, BufWriter, Write },
    path::{ Path, PathBuf },
    fs::{ OpenOptions, File },
};

mod error;
pub use crate::error::{ Result, TodoError };

#[derive(Debug)]
pub struct Todo {
    pub stack: Vec<String>,
    filepath: PathBuf,
}

impl Todo {
    pub fn load(&mut self) -> Result<()> {
        let f = File::open(&self.filepath)?;
        let reader = BufReader::new(f);

        for line in reader.lines() {
            let line = line?.trim().to_string();
            if line == "" { continue; }

            self.stack.push(line);
        }
        Ok(())
    }

    #[allow(dead_code)]
    pub fn save(&mut self) -> io::Result<()> {
        let f = File::create(&self.filepath)?;
        let mut writer = BufWriter::new(f);

        for line in &self.stack {
            writeln!(&mut writer, "{}", line)?;
        }
        writer.flush()?;
        Ok(())
    }

    fn create_file(&mut self) -> io::Result<()> {
        OpenOptions::new().create(true).write(true).open(&self.filepath)?;
        Ok(())
    }
    
    pub fn new(path: &Path) -> Result<Todo> {
        // Todo { stack: vec![], filename }
        let mut todo = Todo { 
            stack: vec![],
            filepath: PathBuf::from(path)
        };

        if !todo.filepath.is_file() {
            todo.create_file()?;
        } else {
            todo.load()?;
        }

        Ok(todo)
    }

    pub fn reset(&mut self) {
        self.stack = vec![];
    }

    pub fn done(&mut self) -> Option<String> {
        self.stack.pop()
    }

    // Swaps two tasks
    pub fn swap(&mut self, depth: usize) -> Result<()> {
        if depth <= 0 {
            Err(TodoError::StackOutOfRange)
        } else if depth > self.stack.len() {
            Err(TodoError::StackUnderflow)
        } else {
            let el = self.stack.swap_remove(self.stack.len() - depth);
            self.stack.push(el);
            Ok(())
        }
    }

    pub fn current_task(&self) -> Result<String> {
        if self.stack.len() == 0 {
            Err(TodoError::NoTask)
        } else {
            Ok(self.stack[self.stack.len() - 1].clone())
        }
    }

    pub fn add(&mut self, s: &str, priority: bool) {
        if priority {
            self.stack.push(String::from(s));
        } else {
            self.stack.insert(0, String::from(s));
        }
    }
}
