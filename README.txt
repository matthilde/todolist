todolist - simple todo list written in rust

This is another simple project to practice rust programming.
There are a couple commands you can find out by typing `todolist help`

This is released under public domain.
